<?php

// This class contain all the request possible for the API
class User{

    // Connection instance
    private $conn;

    // table name
    private $table_name = "user";

    // table columns
        public $id;
        public $nom;
        public $pseudo;
        public $mail;
        public $pass;


// init of constructor
    public function __construct($conn){
        $this->conn = $conn;
    }

/**
* Prepare the SQL request for the API, to get the informations of "user"
*
* @param none
*
* @return string
*/

  public function getOne(){
    // init of $query
    $query = null;

    // check if the user search an spcific id
    if (isset ($_GET['id']) == true) {

      $idGet = $_GET['id'];
      $query = "SELECT id,nom,pseudo,mail,pass FROM " . $this->table_name . " WHERE id = '". $idGet."'";

      // check if the user search an specific pseudo
    }   elseif (isset ($_GET['pseudo']) == true) {

         $pseudoGet = $_GET['pseudo'];
         $query = "SELECT id,nom,pseudo,mail,pass FROM " . $this->table_name . " WHERE pseudo = '". $pseudoGet."'";

         // check if the user search an specific name
    }   elseif (isset ($_GET['nom']) == true) {

        $nomGet = $_GET['nom'];
        $query = "SELECT id,nom,pseudo,mail,pass FROM " . $this->table_name . " WHERE nom = '". $nomGet."'";

    }
      // prepare the query
          $stmt = $this->conn->prepare($query);
      // execute the query
          $stmt->execute();
      // return the query
          return $stmt;
  }

  /**
  * Prepare the request in SQL for the API, to create a new "user"
  *
  * @param none
  *
  * @return string
  */

  public function post(){

      $query = "INSERT INTO " . $this->table_name . " SET nom=:nom, pseudo=:pseudo, mail=:mail, pass=:pass";

      $stmt = $this->conn->prepare($query);

      $stmt->bindParam(":nom", $this->nom);
      $stmt->bindParam(":pseudo", $this->pseudo);
      $stmt->bindParam(":mail", $this->mail);
      $stmt->bindParam(":pass", $this->pass);

      $stmt->execute();

      return $stmt;
  }

  /**
  * Prepare the SQL request for the API, to update the informations of "user"
  *
  * @param none
  *
  * @return string
  */

  public function put(){
    $query = "UPDATE " . $this->table_name . " SET nom=:nom, pseudo=:pseudo, mail=:mail, pass=:pass
              WHERE id=:id";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // bind new values
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':nom', $this->nom);
        $stmt->bindParam(':pseudo', $this->pseudo);
        $stmt->bindParam(':mail', $this->mail);
        $stmt->bindParam(':pass', $this->pass);

        // execute the query
        if($stmt->execute()){
            return true;
        }

        return false;
  }

  /**
  * Prepare the SQL request for the API, to delete a "user"
  *
  * @param none
  *
  * @return string
  */

  public function delete(){
    $query = "DELETE FROM " . $this->table_name . " WHERE id = " .$this->id;

    $stmt = $this->conn->prepare($query);

    $stmt->execute();

    return $stmt;
  }
}
