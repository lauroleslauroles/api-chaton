<?php
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../database.php';
include_once '../../chaton/chaton.php';
include_once 'checker.php';

// init database
$dbclass = new Database();
// launch the database
$conn = $dbclass->getConnection();

// init Chaton class
$chaton = new Chaton($conn);

// decode the json request
$data = json_decode(file_get_contents("php://input"));
$checker = new Checker($data);

$chaton->id = $data->id;
$chaton->couleur = $checker->security($data->couleur);
$chaton->age = $data->age;
$chaton->race = $checker->security($data->race);
$chaton->img = $data->img;

// check if the color exist
if($checker->colorCheck($data) == true){
  // check if the race exist
  if ($checker->raceCheck($data) == true) {
    // update the product
    if($chaton->put()){

        // set response code - 200 ok
        http_response_code(200);

        // tell the user
        echo json_encode(array("message" => "Chaton modified;."));
    }

    // if unable to update the product, tell the user
    else{

        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => "Unable to update product."));
    }
  }  else {
      echo '"This race doesnt exist"';
  }
} else {
  echo "This color doesnt exist";
}
?>
