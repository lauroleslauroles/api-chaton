<?php

class Checker
{
  /**
   * check the color between 'roux', 'noir', 'blanc', for check if this color exists
   *
   *@param data the variable who need to be checked
   *
   *@return string
   */
  function colorCheck($data)
  {
    if ($data->couleur == 'roux' || $data->couleur == 'noir' || $data->couleur == 'blanc') {
      $checker = true;
    }else {
      $checker = false;
    }

    return $checker;

  }
  /**
   * check the color between 'siamois', 'sphynx', 'européen', for check if this race exists
   *
   *@param data the variable who need to be checked
   *
   *@return string
   */
  function raceCheck($data)
  {
    if ($data->race == 'siamois' || $data->race == 'sphynx' || $data->race == 'européen') {
      $checker = true;
    }else {
      $checker = false;
    }

    return $checker;
  }

  /**
   * secure the variable, who can contain some dangerous script, or unknown caracters
   *
   *@param data the variable who need to be check
   *
   *@return string
   */
  function security($data)
  {
     $secure = trim(htmlspecialchars($data));

     return $data;
  }
}


 ?>
