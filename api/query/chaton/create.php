<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../database.php';
include_once '../../chaton/chaton.php';
include_once 'checker.php';

// init database
$dbclass = new Database();
// connect to database
$conn = $dbclass->getConnection();
// Set up a new Chaton
$chaton = new Chaton($conn);
// decode the json query
$data = json_decode(file_get_contents("php://input"));
// set up the class for secure the informations
$checker = new Checker($data);

$chaton->couleur = $checker->security($data->couleur);
$chaton->age = $data->age;
$chaton->race =$checker->security($data->race);
$chaton->img = $data->img;

// check if the color exist
if($checker->colorCheck($data) == true){
  // check if the race exist
  if ($checker->raceCheck($data) == true) {
    // create the new chaton
    if($chaton->post()){

        echo '{';
            echo '"message": "Product was created."';
        echo '}';
    }  else{
        echo '{';
            echo '"message": "Unable to create product."';
        echo '}';
    }
  }  else {
      echo '"This race doesnt exist"';
  }
} else {
  echo "This color doesnt exist";
}


?>
