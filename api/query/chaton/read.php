<?php
header("Content-Type: application/json; charset=UTF-8");

include_once '../../database.php';
include_once '../../chaton/chaton.php';

// init database
$dbclass = new Database();
// connect to database
$conn = $dbclass->getConnection();

// creation of user
$chaton = new Chaton($conn);
// init the query get
$stmt = $chaton->getOne();
// count all the result
$count = $stmt->rowCount();

// check if some user is find
if($count > 0){

// set up an array
    $chatons = array();
    $chatons["body"] = array();
    $chatons["count"] = $count;

// loop while user is find
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);
// set up an array who contain all the information of chaton
        $data  = array(
              "id" => $id,
              "couleur" => $couleur,
              "age" => $age,
              "race" => $race,
              "img" => $img
        );

        array_push($chatons["body"], $data);
    }
// transform into json
    echo json_encode($chatons);
}

else {
// encode the message if no user is find
    echo json_encode(
        array("body" => array(), "count" => 0, "message" => "Aucun chaton avec cette caract&eacute;ristique.")
    );
}
?>
