<?php
header("Content-Type: application/json; charset=UTF-8");

include_once '../../database.php';
include_once '../../user/user.php';

// init database
$dbclass = new Database();
// connection to database
$conn = $dbclass->getConnection();

// creation of user
$user = new User($conn);
// init the query get
$stmt = $user->getOne();
// count all the result
$count = $stmt->rowCount();

// check if some user is find
if($count > 0){

// set up an array
    $users = array();
    $users["body"] = array();
    $users["count"] = $count;

// loop while user is find
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);
// set up an array who contain all the information of user
        $data  = array(
              "id" => $id,
              "nom" => $nom,
              "pseudo" => $pseudo,
              "mail" => $mail,
              "pass" => $pass
        );

        array_push($users["body"], $data);
    }
// transform into json
    echo json_encode($users);
}

else {
// encode the message if no user is find
    echo json_encode(
        array("body" => array(), "count" => 0, "message" => "No user with this feature.")
    );
}
?>
