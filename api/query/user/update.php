<?php
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../database.php';
include_once '../../user/user.php';
include_once 'checker.php';

// init database
$dbclass = new Database();
// launch the database
$conn = $dbclass->getConnection();

// init User class
$user = new User($conn);

// decode the json request
$data = json_decode(file_get_contents("php://input"));
$checker = new Checker($data);

$user->id = $data->id;
$user->nom =  $checker->security($data->nom);
$user->pseudo =  $checker->security($data->pseudo);
$user->mail =  $checker->security($data->mail);
$user->pass =  $checker->security($data->pass);

// update the product
if($user->put()){

    // set response code - 200 ok
    http_response_code(200);

    // tell the user
    echo json_encode(array("message" => "User modified."));
}

// if unable to update the product, tell the user
else{

    // set response code - 503 service unavailable
    http_response_code(503);

    // tell the user
    echo json_encode(array("message" => "Unable to update product."));
}
?>
