<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../database.php';
include_once '../../user/user.php';
include_once 'checker.php';

// init database
$dbclass = new Database();
// connect to database
$conn = $dbclass->getConnection();
// User class
$user = new User($conn);
// decode the json query
$data = json_decode(file_get_contents("php://input"));
// set up the class for secure the informations
$checker = new Checker($data);

      $user->nom = $checker->security($data->nom);
      $user->pseudo = $checker->security($data->pseudo);
      $user->mail = $checker->security($data->mail);
      $user->pass = $checker->security($data->pass);

// create a new user
if($user->post()){
    echo '{';
        echo '"message": "User created."';
    echo '}';
}
// do a message if the user as a problem
else{
    echo '{';
        echo '"message": "Unable to create product."';
    echo '}';
}

?>
