<?php

class Checker
{

  /**
   * secure all the variable, who can contain a script, or some dangerous caracters
   *
   *@param data the variable to secure
   *
   *@return array
   */
  function security($data)
  {
     $secure = trim(htmlspecialchars($data));

     return $secure;
  }
}


 ?>
