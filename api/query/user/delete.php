<?php
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../database.php';
include_once '../../user/user.php';

// init database
$dbclass = new Database();
// connection to database
$conn = $dbclass->getConnection();
// set up new User
$user = new User($conn);
// decode the json query in the variable data
$data = json_decode(file_get_contents("php://input"));

$user->id = $data->id;
// set up a message if the user is deleted
if($user->delete()){

  http_response_code(200);

  echo json_encode(array("message" => "User deleted;."));
}
// set up a message if the user is not find
else{
  http_response_code(500);

  echo json_encode(array("message" => "Unable to delete."));
}


 ?>
