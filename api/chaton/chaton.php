<?php

// This class contain all the request possible for the API
class Chaton{

    // Connection instance
    private $conn;

    // table name
    private $table_name = "chaton";

    // table columns
        public $id;
        public $couleur;
        public $age;
        public $race;
        public $img;


// init of constructor
    public function __construct($conn){
        $this->conn = $conn;
    }

/**
* Prepare the SQL request for the API, to get the informations of "chaton"
*
* @param none
*
* @return string
*/

  public function getOne(){
    // init of $query
    $query = null;

    // check if the user search an spcific id
    if (isset ($_GET['id']) == true) {

      $idGet = $_GET['id'];
      $query = "SELECT id,couleur,age,race,img FROM " . $this->table_name . " WHERE id = ". $idGet;

      // check if the user search an specific color
    }   elseif (isset ($_GET['couleur']) == true) {

         $couleurGet = $_GET['couleur'];
         $query = "SELECT id,couleur,age,race,img FROM " . $this->table_name . " WHERE couleur = '". $couleurGet."'";

         // check if the user search an specific age
    }   elseif (isset ($_GET['age']) == true) {

        $ageGet = $_GET['age'];
        $query = "SELECT id,couleur,age,race,img FROM " . $this->table_name . " WHERE age = '". $ageGet."'";

        // check if the user search an specific race
    }   elseif (isset ($_GET['race']) == true) {

        $raceGet = $_GET['race'];
        $query = "SELECT id,couleur,age,race,img FROM " . $this->table_name . " WHERE race = '". $raceGet."'";

    }
      // prepare the query
          $stmt = $this->conn->prepare($query);
      // execute the query
          $stmt->execute();
      // return the query
          return $stmt;
  }

  /**
  * Prepare the request in SQL for the API, to create a new "chaton"
  *
  * @param none
  *
  * @return string
  */

  public function post(){

      // Set up the query
      $query = "INSERT INTO " . $this->table_name . " SET couleur=:couleur, age=:age, race=:race, img=:img";

      $stmt = $this->conn->prepare($query);

      // bind all parameters
      $stmt->bindParam(":couleur", $this->couleur);
      $stmt->bindParam(":age", $this->age);
      $stmt->bindParam(":race", $this->race);
      $stmt->bindParam(":img", $this->img);

      // execute the query
      $stmt->execute();

      return $stmt;
  }

  /**
  * Prepare the SQL request for the API, to update the informations of "chaton"
  *
  * @param none
  *
  * @return string
  */

  public function put(){
    $query = "UPDATE " . $this->table_name . " SET couleur=:couleur, age=:age, race=:race, img=:img
              WHERE id=:id";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // bind new values
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':couleur', $this->couleur);
        $stmt->bindParam(':age', $this->age);
        $stmt->bindParam(':race', $this->race);
        $stmt->bindParam(':img', $this->img);

        // execute the query
        if($stmt->execute()){
            return true;
        }

        return false;
  }

  /**
  * Prepare the SQL request for the API, to delete a "chaton"
  *
  * @param none
  *
  * @return string
  */

  public function delete(){
    $query = "DELETE FROM " . $this->table_name . " WHERE id = " .$this->id;

    $stmt = $this->conn->prepare($query);

    $stmt->execute();

    return $stmt;
  }
}
